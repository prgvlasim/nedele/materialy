# 28.10.2019 - Git, Display, Position

## Co se dělo
Nastavili jste si Git a vyzkoušeli si jak vytvořit commit a nahrát ho do repozitare na serveru (Gitlab). Ukázal jsem vám univerzální elementy `div` a `span`. Vysvětli jsem vám v CSS box model (display) a pozicování.

## Git
Je systém pro správu verzí (VCS - version control system). Stáhnete z https://git-scm.com/ kde se o něm můžete i všechno dočíst. Návod v češtině zde: https://www.itnetwork.cz/software/git/git-tutorial-historie-a-principy (**doporučuju nastudovat**)

Git se primárně používá přes příkazouvou řádku, nicméně jsem vás učil jak ho používat přes VS Code: [klonování repozitáře](https://code.visualstudio.com/docs/editor/versioncontrol#_cloning-a-repository), [commitování](https://code.visualstudio.com/docs/editor/versioncontrol#_commit), [pushování změn na server](https://code.visualstudio.com/docs/editor/versioncontrol#_remotes)

## Div & span
Dva univerzální elementy, kterými lze obalit libovolný obsah a pak ho stylovat pomocí CSS. Ve výchozím stavu nemají žádné styly, kromě toho, že `span` je řádkový (`display: inline`) a `div` je blokový (`display: block`).

## CSS box model
Na každý HTML element se aplikuje tzv. [box model](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/The_box_model)
![](https://mdn.mozillademos.org/files/16558/box-model.png).

### Na co pamatovat
- uvědomit si rozdíl chování řádkových a blokových elementů (viz [box-model.html](box-model.html))
- typ elementu lze měnit pomocí vlastnosti `display` (hodnoty: `inline`, `block`, `inline-block`)
- ve výchozím stavu se šířka a výška (`width` a `height`) aplikuje na "content box" a padding a border se přidávají navrch, takže výsledná šířka boxu je jejich součet
- lze použít css pravidlo `box-sizing: border-box` pro změnu tohoto chování, kdy šířka a výška se aplikují na "border box", tedy šířka "content boxu" se dopočítá odčtením šířky borderu a paddingu

## Pozicování
Každý element má nějaké [pozicování](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Positioning). Typ pozicování zle měnit pomocí vlastnosti `position`.

- Ve výchozím stavu je `static`, tedy zobazuje se na pozici tak jak je umístěn v dokumentu.
- Hodnota `relative` umožnujě pozicovat element relativně vůči svému výchozímu postavení a to pomocí vlastností `top`, `left`, `right` a `bottom`. *Všimni si*, že na původní pozici po sobě nechává prostor (*ducha*)
- Hodnota `absolute` umožnujě pozicovat element absolutně na konkrétni pozici pomocí vlastností `top`, `left`, `right` a `bottom`. *Všimni si*, že po sobě na výchozí pozici, žádný prostor nenechává.

	> Pozicuje se relativně vůči nejbližšímu nadřazenému (obalujícímu) elementu s vlastností `position` jinou než `static`

- Hodnota `fixed` se chová stejně jako `absolute`, ale pozice je rativně vůči oknu a nemění se při scrollování.

Prozkoumej v [position.html](position.html).

> Existuje i hodnota `sticky`, ale o té někdy příště (vyžaduje specifické podmínky)
