# 13.10.2019 - Základy HTML

## Co se dělo
Mluvili jsme o tom co se děje při zadání www adresy do prohlížeče. Jak vypadá HTML dokument, jak se zapisuje a jaké jsou jeho hlavní elementy. Kde najít základní HTML šablonu a zkusili si něco jednoduchého vytvořit.

> Jelikož nechci vymýšlet kolo a psát novou učebnici, neboť už vše bylo mnohokrát kvalitně napsáno, sepíšu vám základní body, na které je dobré si pamatovat a odkážu vás na zdroj, kde jsem čerpal.

Zdroj: https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Getting_started
> Je to anglicky, tak použij translátor, pokud je to pro tebe překážka

## Elementy
Elementy jsou prvky HTML mající v kontextu dokumentu určitý sémantický význam (nadpis, odstavec, obrázek, odkaz, ...)

- Jak vypadá zípis HTML elementu (https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Getting_started#Anatomy_of_an_HTML_element)

	![](https://mdn.mozillademos.org/files/9347/grumpy-cat-small.png)

- Jak se zapisují komentáře (https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Getting_started#HTML_comments)

- Rozdíl mezi párovým a nepárovým tagem (https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Getting_started#Empty_elements)

	```html
	<p>text</p> <!-- párový -->
	<img src="..."> <!-- nepárový -->
	```

- Dvě možnosti zápisu nepárového tagu

	```html
	<img src="...">
	<img src="..." /> <!-- s uzavírací značkou -->
	```

- Elementy lze do sebe vnořovat (https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Getting_started#Nesting_elements)

- Rozdíl mezi blokovým (block) a řádkovým (inline) elementem (https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Getting_started#Block_versus_inline_elements)

- Bílé znaky (mezery, zalomení, ...) se v html povětšinou ignorují (https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Getting_started#Whitespace_in_HTML)
	> **TIP**: existuje HTML element, kde tohle neplatí, kdo ho najde má zlatýho bludišťáka :-)

# Atributy
Elementy mají atributy, kterými lze elementům nastavit učité hodnoty, které nejsou přímo součástí jejich obsahu.

- Zápis vypadá takto (https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Getting_started#Attributes)

	![](https://mdn.mozillademos.org/files/9345/grumpy-cat-attribute-small.png)

- Hodnota atributu se zpravidla zapisuje do dvojitých uvozovek `"`, ale lze i do jednoduchých `'` a nebo je uplně vynechat, ale není to doporučené (může to vést k chybám) (https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Getting_started#Omitting_quotes_around_attribute_values)

- Existují logické atributy (https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Getting_started#Boolean_attributes)

# Struktura HTML dokumentu
https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Getting_started#Anatomy_of_an_HTML_document

# Základní elementy
- nadpis: `<h1>` až `<h6>`
- odstavec: `<p>`
- důraz: `<em>`, `<strong>`
- obrázek: `<img>`
- vynucené zalomení řádku: `<br>`
- odkaz: `<a>`

> **TIP**: tady můžeš objevovat další: https://developer.mozilla.org/en-US/docs/Web/HTML/Element

# Jak začít s tvorbou HTML šablony?
Pokud si jako já nepamatuješ přesně, jak vypadá základní struktura HTML se všemi důležitými elementy, nebo jsi jen líný psát, tak si ji vyhledej na internetu. Vygoogli např. *basic html template*. První odkaz: https://www.sitepoint.com/a-basic-html5-template/

# Úkol: Vytvoř si základní HTML stránku
Vytvoř si základní HTML stránku, kde použiješ elementy pro nadpis, odstavec, obrázek. Zkoušej měnit atributy (např. rozměry obrázku). A neboj se experimentovat a použij i nějaký tag, o kterém jsme nemluvili.

Stránku si ulož, ať s ní můžeš příště pracovat a rozšiřovat ji.

> Seznam a popis jednotlivých tagů najdeš tady: https://developer.mozilla.org/en-US/docs/Web/HTML/Element
