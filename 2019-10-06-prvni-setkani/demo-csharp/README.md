# Demo C#

## Nainstaluj si .Net Core SDK

Stáhni a nainstaluj si .Net Core SDK ze stránky https://dotnet.microsoft.com/download (musí to být SDK, ne Runtime nebo něco jiného)

## Spusť projekt

1. Spusť konzoli (cmd, PowerShell)
	> pokud nevíš, klikni na start, napiš `cmd` a stiskni Enter
2. V konzoli přejdi do složky s projektem (obsahuje soubor `*.csproj`)
	> napiš `cd C:\cesta\do\složky\projektu`, stiskni Enter
3. Spusť projekt
	> napiš `dotnet run`, stiskni Enter

## Hraj si :-)

Uprav kód pokud chceš a spusť projekt a uvidíš výsledek.
