# První setkání 6.10.2019

## Co se dělo

Řekli jsme si vzájemně svoje očekávání a dohodli se, že se budem scházet v **neděli ve 14:00**. Ukázal jsem vám jak vypadá programování v jazyku C# a tvorba HTML šablony, abyste si udělali představu.

## Úkol 1: Vývojářské prostředí

Nainstaluj si nějaké vývojářské prostředí. Pokud nevíš jaké, doporučuju [Visual Studio Code](https://code.visualstudio.com/).

## Úkol 2: Vyzkoušej si

Ve složkách [demo-csharp](demo-csharp) a [demo-html-css](demo-html-css) najdeš zdrojové soubory ukázek co jsem vám předváděl. Tak si je zkus spustit. U každého projektu je i návod jak na to.

## Úkol 3: Založ si účet na GitLabu

Zaregistruj se https://gitlab.com/users/sign_in a pošli mi uživatelský jméno.
