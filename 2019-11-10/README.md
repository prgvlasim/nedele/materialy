# 10.11.2019 - layout, odkazy, hover

## Co se dělo
Pokračovali jsme na ukázce z minula. Vyzkoušeli jsme si jak se pracuje s odkazy na podstránky a s hashem, bavili se ještě o pozicování a o hover efektu.

## Úkol: Životopis
Jelikož příště lekce nebude, mám pro tebe takovou challenge. Nakódoval jsem stránku se životopisem

![](ukol/screenshot.png)

A tvým úkolem bude co nejvěrněji napodobit, pouze zaměň údaje a fotku za svoje (nemusí být pravdivý). Možná si nebudeš s něčím vědět rady, ale to vůbec nevadí, udělej co dokážeš.

V neděli 17.11. se podívám co už máš, jinak je deadline do 24.11.

> Nejlepší bude, když **nebudete** spolupracovat, ale každý to zkusí **sám**, jedině tak to má skutečný užitek. Pokud si nebudš vědět s něčím, rady, tak mi napiš nebo zavolej, já tě odkážu, kde zjistíš odpověd.

Ve složce [ukol](ukol) najdeš předpřipravený dokument (v podstatě jen vložené písmo, o to se nemusíš starat). A ve složce [ukol/icons](ukol/icons) najdeš použité ikonky.

### CSS vlastnosti co se ti budou hodit
- position
- margin
- padding
- border
- border-radius
- font-size
- font-style
- font-weight
- text-transform
- opacity
- box-shadow

### Barvy
- pozadí: `#393c41`
- levý sloupec: `rgba(0, 0, 0, .05)`
- zlaté nadpisy: `#b89763`
- černé nadpisy: `rgba(0, 0, 0, .72)`

### Rozměry
- stránka životopisu (všetně šedého sloupce): `800 x 1000px`
- šířka levého sloupce: `300px`
- šířka fotky: `160px`
- vnitřní okraje sloupce a obsahu: `40px`

### Tipy

- Začni rozvržením, tedy nejdřív vytvoř samotnou stránku, vycentruj, pak levý slupec, ...

- Vnímej dokument blokově a pro podobné bloky používej stejné CSS třídy abys nemusel/a stylovat každý kus textu zvlášť:

![](ukol/outline.png)

- Šetři s absolutním pozicováním (`position: absolute`). Já ho použil 2x a chápal bych ještě jedno použití navíc. Jinak vše lze řašit pomocí `margin`, `padding`, `width` a `height`. Pokud použiješ absolutní pozicování, budeš nejspíš potřebovat i `position: relative` na nějaký nadřazený prvek, vůči kterému se ten absolutní pozicuje.

- `position: fixed` není třeba vůbec,

- Pokud potřebuješ vycentrovat obsah elementu na výšku, nastav mu tyhle vlastnosti (někdy později vysvetlím):

```css
.box {
	display: flex;
	flex-direction: column;
	justify-content: center;
}
```


### Založ si repozitář
Založ si nový repozitář (*Gitlab -> new project*), kam budeš nahrávat kód. Dej mi práva, ať můžu vidět kód (*Settings -> Members*, role *Developer*). A nahrávej průběžně změny.
