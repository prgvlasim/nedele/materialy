# 5.11.2019 - rozvržení stránky (layout)

## Co se dělo
Ukázal jsem vám jak vytvořit jedno ze základních rozvržení stránky, které obsahuje horní lištu, menu a obsah.

## Úkol: Stáhni si změny v repozitáři *Materiály*
To jak naklonovat repozitář jsem vám vysvětlil (`Git: Clone` v VS Code). Nestihl jsem vám ale ukázat jak si stáhnout nové změny. Je to ale snadný, takže si to doma vyzkoušej.

Tady je postup jak na to:
1. Spusť VS Code
2. Otevři v něm složku, do které sis naklonoval/a repozitář [*Materiály*](https://gitlab.com/prgvlasim/nedele/materialy)
3. Přepni se do *Source control* panelu:

	![](./.readme/source-control-panel.png)

4. V menu (tři tečky) klikni na položku *Pull*:

	![](./.readme/pull.png)

Pak chvíli počkej a je hotovo. Ve složce pak budeš mít soubory odpovídající aktuálnímu stavu na serveru. Měl/a bys tedy vidět zdrojové kódy které jsem tvořil během lekce.

> **Pamatuj**: Pokud, chceš něco upravovat, nejdřív si to vykopíruj někam mimo složku s repozitářem *Materiály*, jinak může při stahování nových změn nastat problém s kolizí s tvými úpravami

### Možný problém: Kolidující úpravy

Pokud se ti stane, že při kliknutí na *Pull* ti vyskočí tahle hláška:

![](./.readme/clean.png)

Znamená to, že si něco upravoval/a přímo v repozitáři *Materiály* a stažení nových změn by tvoje úpravy přepsalo. Musíš tedy svoje úpravy z repozitáře ostranit (pokud si je chceš zachovat, tak si je někam překopíruj).

Přehled změn najdeš v panelu *Source Control* (viz bod 3).

Všechny změny zahodíš přes položku v menu (tři tečky) *Discard All Changes*:

![](./.readme/discard.png)
